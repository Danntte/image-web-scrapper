<?php
require 'vendor/autoload.php';

use GuzzleHttp\Client;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use GuzzleHttp\Psr7\Utils;
use GuzzleHttp\Psr7\Uri;

$reader = new Xlsx();

$spreadsheet = $reader->load("test.xlsx");

$items=$spreadsheet->getSheet(0)->toArray();

$oisid = 'dbb2b54f0ef7bb5613af35e98f86f88e';

$numItems = count($items);
$count = 0;
foreach ($items as $item){
    $count++;
    $httpClient = new Client([
        // Base URI is used with relative requests
        'base_uri' => 'https://admin.originalimpressions.com/',
    ]);
    $response = $httpClient->get('/admin/viewPop.php?oisid='.$oisid.'&itemid='.$item[0].'&type=main');
    $htmlString = (string) $response->getBody();
    //Convert string to XML
    libxml_use_internal_errors(true);
    $doc = new DOMDocument();
    $doc->loadHTML($htmlString);
    $xpath = new DOMXPath($doc);
    $titles = $xpath->evaluate('//center//img/@src');

    foreach ($titles as $title) {

        $destination = fopen('imgs/AV'.$item[0].'.jpg', 'w+');
        $source = trim($title->textContent.PHP_EOL[0]);
        
        //New client to download img.
        $client = new Client();
        $response = $client->get($source, ['sink' => $destination]);
        
        fclose($destination);
        echo 'Item '.$count.' of '.$numItems.' AV'.$item[0].'
';
    }
}